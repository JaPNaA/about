declare let utils: {
    /**
     * gets the distance from point to 0, 0
     * @param x
     * @param y
     */
    pytha(x: number, y: number): number;
    /**
     * gets the sign (+/-) of number
     * @param x
     */
    sign(x: number): number;
    /**
     * squares while preserving sign
     * @param x number to square
     */
    signedSquare(x: number): number;
    /**
     * splits a string before delimeter
     * @param s string to split
     * @param d delimeter
     */
    splitBefore(s: string, d: RegExp): string[];
};
export default utils;
//# sourceMappingURL=utils.d.ts.map