declare class HexagonIco {
    elm: HTMLImageElement;
    x: number;
    y: number;
    translateX: number;
    translateY: number;
    size: number;
    angle: number;
    /**
     * Hexagon Ico constructor
     * @param isMobile is user on mobile?
     */
    constructor(isMobile: boolean);
    mousemoveHandler(e: MouseEvent): void;
    setPos(x: number, y: number): void;
    setTranslatePos(x: number, y: number): void;
    setSize(size: number): void;
    setAngle(angle: number): void;
    appendTo(elm: HTMLElement): void;
}
declare class Components {
    static flags: {
        mobile: boolean;
    };
    static hexagonDistributionConfig: {
        pointX: number;
        pointY: number;
        pointPow: number;
        pointRadius: number;
        scalar: number;
        amount: number;
        layers: number;
    };
    static flagNotMobile(): void;
    static createHexagonIcon(): HexagonIco;
    static appendHexagonsDistribution(elm: HTMLElement): void;
    static createHexagonLayer(z: number): HTMLDivElement;
    static appendJaPNaALogo(elm: HTMLElement): void;
    static createTitleElm(): HTMLDivElement;
    static createBodyElm(): HTMLDivElement;
}
export default Components;
//# sourceMappingURL=components.d.ts.map