var Networker = /** @class */ (function () {
    function Networker() {
    }
    Networker.init = function () {
        this.nocacheFetchInit = {
            method: "GET",
            headers: this.nocacheHeader
        };
        this.nocacheHeader.append("pragma", "no-cache");
        this.nocacheHeader.append("cache-control", "no-cache");
    };
    Networker.registerRequest = function (req) {
        var _this = this;
        req.then(function (e) {
            _this.doneRequests.push(req);
            _this.pendingRequests.splice(_this.pendingRequests.indexOf(req), 1);
            return e;
        });
        this.pendingRequests.push(req);
    };
    Networker.get = function (path) {
        var req = fetch(path);
        this.registerRequest(req);
        return req;
    };
    Networker.getFresh = function (path) {
        var req = fetch(path, this.nocacheFetchInit);
        this.registerRequest(req);
        return req;
    };
    Networker.pendingRequests = [];
    Networker.doneRequests = [];
    Networker.nocacheHeader = new Headers();
    return Networker;
}());
Networker.init();
export default Networker;
//# sourceMappingURL=networker.js.map