declare class Networker {
    static pendingRequests: Promise<Response>[];
    static doneRequests: Promise<Response>[];
    private static nocacheHeader;
    private static nocacheFetchInit;
    static init(): void;
    private static registerRequest;
    static get(path: string): Promise<Response>;
    static getFresh(path: string): Promise<Response>;
}
export default Networker;
//# sourceMappingURL=networker.d.ts.map