import components from "./components.js";
import checkMobile from "./checkmobile.js";
var App = /** @class */ (function () {
    function App() {
        this.isMobile = checkMobile();
        this.main = document.createElement("div");
        if (!this.isMobile) {
            components.flagNotMobile();
        }
        this.title = components.createTitleElm();
        this.body = components.createBodyElm();
        this.setup();
    }
    App.prototype.setup = function () {
        this.main.appendChild(this.title);
        this.main.appendChild(this.body);
        // {
        //     const div = document.createElement("div");
        //     div.style.height = "1000px";
        //     this.main.appendChild(div);
        // }
        this.main.classList.add("main");
        document.body.appendChild(this.main);
    };
    return App;
}());
var app = new App();
//# sourceMappingURL=index.js.map