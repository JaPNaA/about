import utils from "./utils.js";
var GroupedElement = /** @class */ (function () {
    /**
     * GroupedElement constructor
     * @param appendFrom Element that will be appended
     * @param appendTo Element that other elements will append to
     */
    function GroupedElement(appendFrom, appendTo) {
        this.root = appendFrom;
        this.childAppender = appendTo;
    }
    GroupedElement.prototype.appendTo = function (element) {
        element.appendChild(this.root);
    };
    GroupedElement.prototype.appendChild = function (element) {
        this.childAppender.appendChild(element);
    };
    return GroupedElement;
}());
var Parser = /** @class */ (function () {
    function Parser() {
    }
    Parser.parseGroupedElmTag = function (tag) {
        var elms = tag.split(/\s+/);
        var regex = /[\.#]/;
        var rootElement = null;
        var appendixElement = null;
        for (var _i = 0, elms_1 = elms; _i < elms_1.length; _i++) {
            var elm = elms_1[_i];
            var id = void 0;
            var classList = [];
            var tag_1 = void 0;
            var arr = utils.splitBefore(elm, regex);
            for (var _a = 0, arr_1 = arr; _a < arr_1.length; _a++) {
                var i = arr_1[_a];
                switch (i[0]) {
                    case ".":
                        classList.push(i.slice(1));
                        break;
                    case "#":
                        id = i.slice(1);
                        break;
                    default:
                        tag_1 = i;
                        break;
                }
            }
            var $ = void 0;
            if (!tag_1) {
                $ = document.createElement("span");
            }
            else {
                $ = document.createElement(tag_1);
            }
            for (var _b = 0, classList_1 = classList; _b < classList_1.length; _b++) {
                var cls = classList_1[_b];
                $.classList.add(cls);
            }
            if (id) {
                $.id = id;
            }
            if (!rootElement) {
                rootElement = $;
            }
            else if (appendixElement) {
                appendixElement.appendChild($);
            }
            else {
                throw new Error("Unknown Error");
            }
            appendixElement = $;
        }
        if (rootElement && appendixElement) {
            return new GroupedElement(rootElement, appendixElement);
        }
        else {
            throw new EvalError("No elements defined in tag");
        }
    };
    Parser.pipeline_toLines = function (arr) {
        var narr = [];
        for (var _i = 0, arr_2 = arr; _i < arr_2.length; _i++) {
            var i = arr_2[_i];
            // skip if string is nothing or whitespace
            if (i.match(/^\s*$/)) {
                continue;
            }
            var str = i.trim();
            // add string to end of prev
            if (str[0] == "|") {
                if (narr.length) {
                    narr[narr.length - 1] += str.slice(1);
                }
                else {
                    throw new EvalError("Tried to continue previous line with no previous line");
                }
                continue;
            }
            narr.push(i);
        }
        return narr;
    };
    Parser.pipleine_tree = function (arr) {
        var narr = [];
        var expectingData = false;
        var data = [];
        var depth = 0;
        for (var _i = 0, arr_3 = arr; _i < arr_3.length; _i++) {
            var i = arr_3[_i];
            if (expectingData) {
                // check if ends in ", ["
                if (i.match(/,\s*\[\s*$/)) {
                    depth++;
                    // check if line is "]"
                }
                else if (i.match(/^\s*\]\s*$/)) {
                    depth--;
                }
                if (depth === 0) {
                    expectingData = false;
                    narr[narr.length - 1][1] = this.parseTxtToObject(data);
                    continue;
                }
                else if (depth < 0) {
                    throw new EvalError("Syntax Error: unexpected ']'\n\t" + i + "\n");
                }
                data.push(i);
                continue;
            }
            var larr = [];
            var commaIx = i.indexOf(",");
            if (commaIx < 0) {
                throw new EvalError("Syntax Error:\n\t" + i + "\n");
            }
            larr[0] = i.slice(0, commaIx).trim();
            larr[1] = i.slice(commaIx + 1);
            if (larr[1].match(/^\s*\[/)) {
                expectingData = true;
                data = [];
                depth++;
            }
            narr.push(larr);
        }
        return narr;
    };
    Parser.parseTxtToObject = function (txt) {
        // PIPELINE
        var arr;
        if (typeof txt === "string") {
            arr = txt.split("\n");
        }
        else {
            arr = txt;
        }
        arr = this.pipeline_toLines(arr);
        arr = this.pipleine_tree(arr);
        return arr;
    };
    Parser.parse = function (objs) {
        var doc = document.createDocumentFragment();
        if (typeof objs === "string") {
            doc.appendChild(document.createTextNode(objs));
        }
        else {
            for (var _i = 0, objs_1 = objs; _i < objs_1.length; _i++) {
                var thing = objs_1[_i];
                var elm = this.parseGroupedElmTag(thing[0]);
                elm.appendChild(this.parse(thing[1]));
                elm.appendTo(doc);
            }
        }
        return doc;
    };
    /**
     * parses and appends parsed things to elm
     * @param objs object to parse
     * @param elm element to append to
     */
    Parser.parseAndPipeToElm = function (objs, elm) {
        elm.appendChild(this.parse(objs));
    };
    return Parser;
}());
export default Parser;
//# sourceMappingURL=parser.js.map