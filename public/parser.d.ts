declare class Parser {
    private static parseGroupedElmTag;
    static pipeline_toLines(arr: string[]): string[];
    static pipleine_tree(arr: string[]): any[][];
    static parseTxtToObject(txt: string | string[]): Object;
    static parse(objs: any): DocumentFragment;
    /**
     * parses and appends parsed things to elm
     * @param objs object to parse
     * @param elm element to append to
     */
    static parseAndPipeToElm(objs: any, elm: HTMLElement): void;
}
export default Parser;
//# sourceMappingURL=parser.d.ts.map