import utils from "./utils.js";
import networker from "./networker.js";
import parser from "./parser.js";
var paths = {
    hexagon: "assets/hexagon.svg",
    japnaaLogo: "assets/japnaa-logo.svg",
    data: "data/data.txt"
};
var HexagonIco = /** @class */ (function () {
    /**
     * Hexagon Ico constructor
     * @param isMobile is user on mobile?
     */
    function HexagonIco(isMobile) {
        this.x = 0;
        this.y = 0;
        this.translateX = 0;
        this.translateY = 0;
        this.size = 0;
        this.angle = 0;
        this.elm = document.createElement("img");
        this.elm.src = paths.hexagon;
        this.elm.classList.add("hexagon");
        if (!isMobile) {
            addEventListener("mousemove", this.mousemoveHandler.bind(this));
        }
    }
    HexagonIco.prototype.mousemoveHandler = function (e) {
        var mouseX = e.pageX / innerWidth;
        var mouseY = e.pageY / innerHeight;
        // console.log(e.pageY);
        var dx = mouseX - this.x;
        var dy = mouseY - this.y;
        var dist = utils.pytha(dx, dy);
        var ang = Math.atan2(dy, dx) + Math.PI;
        var scale = 2;
        var rad = 0.2;
        if (dist < rad) {
            var idist = rad - dist, idist2 = idist * idist;
            this.setTranslatePos(Math.cos(ang) * idist2 * scale, Math.sin(ang) * idist2 * scale);
        }
        else {
            this.setTranslatePos(0, 0);
        }
    };
    HexagonIco.prototype.setPos = function (x, y) {
        this.elm.style.setProperty("--x-pos", x.toString());
        this.elm.style.setProperty("--y-pos", y.toString());
        this.x = x;
        this.y = y;
    };
    HexagonIco.prototype.setTranslatePos = function (x, y) {
        this.translateX = x;
        this.translateY = y;
        this.elm.style.setProperty("--x-translate", x.toString());
        this.elm.style.setProperty("--y-translate", y.toString());
    };
    HexagonIco.prototype.setSize = function (size) {
        this.elm.style.setProperty("--size", size.toString());
        this.size = size;
    };
    HexagonIco.prototype.setAngle = function (angle) {
        this.elm.style.setProperty("--angle", angle.toString());
        this.angle = angle;
    };
    HexagonIco.prototype.appendTo = function (elm) {
        elm.appendChild(this.elm);
    };
    return HexagonIco;
}());
var Components = /** @class */ (function () {
    function Components() {
    }
    Components.flagNotMobile = function () {
        this.flags.mobile = false;
        this.hexagonDistributionConfig.layers = 3;
    };
    Components.createHexagonIcon = function () {
        return new HexagonIco(this.flags.mobile);
    };
    Components.appendHexagonsDistribution = function (elm) {
        var conf = this.hexagonDistributionConfig;
        for (var i = 0; i < conf.amount; i++) {
            var hexagon = this.createHexagonIcon();
            var x = Math.random();
            var y = Math.random();
            var size = Math.random() * conf.scalar;
            var dx = x - conf.pointX;
            var dy = y - conf.pointY;
            var dist = utils.pytha(dx, dy);
            if (dist < conf.pointRadius) {
                x += utils.sign(dx) * conf.pointRadius * Math.SQRT1_2;
                y += utils.sign(dx) * conf.pointRadius * Math.SQRT1_2;
            }
            dist = utils.pytha(dx, dy);
            size *= Math.pow(dist, conf.pointPow);
            hexagon.setPos(x, y);
            hexagon.setSize(size);
            hexagon.setAngle(Math.random());
            hexagon.appendTo(elm);
        }
    };
    Components.createHexagonLayer = function (z) {
        var layer = document.createElement("div");
        layer.classList.add("hexagonsLayer");
        this.appendHexagonsDistribution(layer);
        if (!this.flags.mobile) {
            // paralax effect on desktops only
            addEventListener("scroll", function () {
                layer.style.setProperty("--scrolled", (z * this.document.body.scrollTop * 0.3).toString());
            }, {
                passive: true
            });
        }
        return layer;
    };
    Components.appendJaPNaALogo = function (elm) {
        var img = document.createElement("img");
        img.src = paths.japnaaLogo;
        img.classList.add("logo");
        elm.appendChild(img);
    };
    Components.createTitleElm = function () {
        var elm = document.createElement("div");
        elm.classList.add("title");
        var hexagonLayers = [];
        for (var i = 0; i < this.hexagonDistributionConfig.layers; i++) {
            var layer = this.createHexagonLayer(i);
            elm.appendChild(layer);
            hexagonLayers.push(layer);
        }
        this.appendJaPNaALogo(elm);
        return elm;
    };
    Components.createBodyElm = function () {
        var elm = document.createElement("div");
        elm.classList.add("body");
        // TEMP
        networker.getFresh(paths.data)
            .then(function (e) { return e.text(); })
            .then(function (e) { return parser.parseTxtToObject(e); })
            .then(function (e) { return parser.parseAndPipeToElm(e, elm); });
        return elm;
    };
    Components.flags = {
        mobile: true
    };
    Components.hexagonDistributionConfig = {
        pointX: 0.5,
        pointY: 0.4,
        pointPow: 2,
        pointRadius: 0.3,
        scalar: 3,
        amount: 50,
        layers: 2
    };
    return Components;
}());
export default Components;
//# sourceMappingURL=components.js.map