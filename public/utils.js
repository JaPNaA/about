var utils = {
    /**
     * gets the distance from point to 0, 0
     * @param x
     * @param y
     */
    pytha: function (x, y) {
        return Math.sqrt(x * x + y * y);
    },
    /**
     * gets the sign (+/-) of number
     * @param x
     */
    sign: function (x) {
        return x < 0 ? -1 : 1;
    },
    /**
     * squares while preserving sign
     * @param x number to square
     */
    signedSquare: function (x) {
        return utils.sign(x) * x * x;
    },
    /**
     * splits a string before delimeter
     * @param s string to split
     * @param d delimeter
     */
    splitBefore: function (s, d) {
        var str = s;
        var arr = [];
        while (str.length > 0) {
            var match = str.slice(1).match(d);
            if (!match) {
                arr.push(str);
                break;
            }
            if (match.index !== undefined) {
                var part = str.slice(0, match.index + 1);
                str = str.slice(match.index + 1);
                arr.push(part);
            }
            else {
                throw new TypeError("RegExp cannot be global");
            }
        }
        return arr;
    }
};
export default utils;
//# sourceMappingURL=utils.js.map