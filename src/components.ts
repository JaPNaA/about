import utils from "./utils.js";
import networker from "./networker.js";
import parser from "./parser.js";

const paths = {
    hexagon: "assets/hexagon.svg",
    japnaaLogo: "assets/japnaa-logo.svg",
    data: "data/data.txt"
};

class HexagonIco {
    elm: HTMLImageElement;

    x: number = 0;
    y: number = 0;

    translateX: number = 0;
    translateY: number = 0;

    size: number = 0;
    angle: number = 0;

    /**
     * Hexagon Ico constructor
     * @param isMobile is user on mobile?
     */
    constructor(isMobile: boolean) {
        this.elm = document.createElement("img");
        this.elm.src = paths.hexagon;
        this.elm.classList.add("hexagon");

        if (!isMobile) {
            addEventListener("mousemove", this.mousemoveHandler.bind(this));
        }
    }

    mousemoveHandler(e: MouseEvent) {
        let mouseX = e.pageX / innerWidth;
        let mouseY = e.pageY / innerHeight;

        // console.log(e.pageY);

        let dx = mouseX - this.x;
        let dy = mouseY - this.y;

        let dist = utils.pytha(dx, dy);
        let ang = Math.atan2(dy, dx) + Math.PI;

        let scale = 2;
        let rad = 0.2;

        if (dist < rad) {
            let idist = rad - dist,
                idist2 = idist * idist;
            this.setTranslatePos(
                Math.cos(ang) * idist2 * scale,
                Math.sin(ang) * idist2 * scale
            );
        } else {
            this.setTranslatePos(0, 0);
        }
    }

    setPos(x: number, y: number) {
        this.elm.style.setProperty("--x-pos", x.toString());
        this.elm.style.setProperty("--y-pos", y.toString());
        this.x = x;
        this.y = y;
    }

    setTranslatePos(x: number, y: number) {
        this.translateX = x;
        this.translateY = y;

        this.elm.style.setProperty("--x-translate", x.toString());
        this.elm.style.setProperty("--y-translate", y.toString());
    }

    setSize(size: number) {
        this.elm.style.setProperty("--size", size.toString());
        this.size = size;
    }

    setAngle(angle: number) {
        this.elm.style.setProperty("--angle", angle.toString());
        this.angle = angle;
    }

    appendTo(elm: HTMLElement) {
        elm.appendChild(this.elm);
    }
}

class Components {
    static flags = {
        mobile: true
    }

    static hexagonDistributionConfig = {
        pointX: 0.5,
        pointY: 0.4,
        pointPow: 2,
        pointRadius: 0.3,
        scalar: 3,

        amount: 50,
        layers: 2
    };

    static flagNotMobile() {
        this.flags.mobile = false;
        this.hexagonDistributionConfig.layers = 3;
    }

    static createHexagonIcon(): HexagonIco {
        return new HexagonIco(this.flags.mobile);
    }

    static appendHexagonsDistribution(elm: HTMLElement): void {
        const conf = this.hexagonDistributionConfig;

        for (let i = 0; i < conf.amount; i++) {
            const hexagon = this.createHexagonIcon();

            let x = Math.random();
            let y = Math.random();
            let size = Math.random() * conf.scalar;

            let dx = x - conf.pointX;
            let dy = y - conf.pointY;
            let dist = utils.pytha(dx, dy);

            if (dist < conf.pointRadius) {
                x += utils.sign(dx) * conf.pointRadius * Math.SQRT1_2;
                y += utils.sign(dx) * conf.pointRadius * Math.SQRT1_2;
            }

            dist = utils.pytha(dx, dy);

            size *= dist ** conf.pointPow;

            hexagon.setPos(x, y);
            hexagon.setSize(size);
            hexagon.setAngle(Math.random());

            hexagon.appendTo(elm);
        }
    }

    static createHexagonLayer(z: number): HTMLDivElement {
        const layer = document.createElement("div");
        layer.classList.add("hexagonsLayer");

        this.appendHexagonsDistribution(layer);

        if (!this.flags.mobile) {
            // paralax effect on desktops only
            addEventListener("scroll", function() {
                layer.style.setProperty("--scrolled", (z * this.document.body.scrollTop * 0.3).toString());
            }, {
                passive: true
            });
        }

        return layer;
    }

    static appendJaPNaALogo(elm: HTMLElement): void {
        const img = document.createElement("img");
        img.src = paths.japnaaLogo;
        img.classList.add("logo");
        elm.appendChild(img);
    }

    static createTitleElm(): HTMLDivElement {
        const elm = document.createElement("div");
        elm.classList.add("title");

        const hexagonLayers = [];

        for (let i = 0; i < this.hexagonDistributionConfig.layers; i++) {
            const layer = this.createHexagonLayer(i);
            elm.appendChild(layer);
            hexagonLayers.push(layer);
        }

        this.appendJaPNaALogo(elm);

        return elm;
    }

    static createBodyElm(): HTMLDivElement {
        const elm = document.createElement("div");
        elm.classList.add("body");

        // TEMP
        networker.getFresh(paths.data)
            .then(e => e.text())
            .then(e => parser.parseTxtToObject(e))
            .then(e => parser.parseAndPipeToElm(e, elm));

        return elm;
    }
}

export default Components;