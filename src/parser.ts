import utils from "./utils.js";

class GroupedElement {
    root: HTMLElement;
    childAppender: HTMLElement

    /**
     * GroupedElement constructor
     * @param appendFrom Element that will be appended
     * @param appendTo Element that other elements will append to
     */
    constructor(appendFrom: HTMLElement, appendTo: HTMLElement) {
        this.root = appendFrom;
        this.childAppender = appendTo;
    }

    appendTo(element: Element | DocumentFragment) {
        element.appendChild(this.root);
    }

    appendChild(element: Element | DocumentFragment) {
        this.childAppender.appendChild(element);
    }
}

class Parser {
    private static parseGroupedElmTag(tag: string): GroupedElement {
        let elms = tag.split(/\s+/);
        let regex = /[\.#]/;
        let rootElement: HTMLElement | null = null;
        let appendixElement: HTMLElement | null = null;
        
        for (let elm of elms) {
            let id: string | undefined;
            let classList: string[] = [];
            let tag: string | undefined;

            let arr = utils.splitBefore(elm, regex);
            for (let i of arr) {
                switch(i[0]) {
                case ".":
                    classList.push(i.slice(1));
                    break;
                case "#":
                    id = i.slice(1);
                    break;
                default:
                    tag = i;
                    break;
                }
            }

            let $: HTMLElement;

            if (!tag) {
                $ = document.createElement("span");
            } else {
                $ = document.createElement(tag);
            }

            for (let cls of classList) {
                $.classList.add(cls);
            }
            if (id) {
                $.id = id;
            }

            if (!rootElement) {
                rootElement = $;
            } else if (appendixElement) {
                appendixElement.appendChild($)
            } else {
                throw new Error("Unknown Error");
            }

            appendixElement = $;
        }

        if (rootElement && appendixElement) {
            return new GroupedElement(rootElement, appendixElement);
        } else {
            throw new EvalError("No elements defined in tag");
        }
    }

    static pipeline_toLines(arr: string[]): string[] {
        let narr: string[] = [];

        for (let i of arr) {
            // skip if string is nothing or whitespace
            if (i.match(/^\s*$/)) {
                continue;
            }

            let str = i.trim();

            // add string to end of prev
            if (str[0] == "|") {
                if (narr.length) {
                    narr[narr.length - 1] += str.slice(1);
                } else {
                    throw new EvalError("Tried to continue previous line with no previous line");
                }
                continue;
            }

            narr.push(i);
        }

        return narr;
    }

    static pipleine_tree(arr: string[]): any[][] {
        let narr: any[] = [];

        let expectingData = false;
        let data = [];
        let depth = 0;

        for (let i of arr) {
            if (expectingData) {
                // check if ends in ", ["
                if (i.match(/,\s*\[\s*$/)) {
                    depth++;
                // check if line is "]"
                } else if (i.match(/^\s*\]\s*$/)) {
                    depth--;
                }

                if (depth === 0) {
                    expectingData = false;
                    narr[narr.length - 1][1] = this.parseTxtToObject(data);
                    continue;
                } else if (depth < 0) {
                    throw new EvalError("Syntax Error: unexpected ']'\n\t" + i + "\n")
                }

                data.push(i);

                continue;
            }

            let larr = [];
            let commaIx = i.indexOf(",");

            if (commaIx < 0) {
                throw new EvalError("Syntax Error:\n\t" + i + "\n");
            }

            larr[0] = i.slice(0, commaIx).trim();
            larr[1] = i.slice(commaIx + 1);

            if (larr[1].match(/^\s*\[/)) {
                expectingData = true;
                data = [];
                depth++;
            }

            narr.push(larr);
        }

        return narr;
    }

    static parseTxtToObject(txt: string|string[]): Object {
        // PIPELINE
        let arr: any[];
        if (typeof txt === "string") {
            arr = txt.split("\n");
        } else {
            arr = txt;
        }

        arr = this.pipeline_toLines(arr);
        arr = this.pipleine_tree(arr);
        return arr;
    }

    static parse(objs: any): DocumentFragment {
        const doc = document.createDocumentFragment();

        if (typeof objs === "string") {
            doc.appendChild(document.createTextNode(objs));
        } else {
            for (let thing of objs) {
                const elm: GroupedElement = this.parseGroupedElmTag(thing[0]);
                elm.appendChild(this.parse(thing[1]));
                elm.appendTo(doc);
            }
        }

        return doc;
    }

    /**
     * parses and appends parsed things to elm
     * @param objs object to parse
     * @param elm element to append to
     */
    static parseAndPipeToElm(objs: any, elm: HTMLElement): void {
        elm.appendChild(this.parse(objs));
    }
}

export default Parser;