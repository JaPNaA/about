import components from "./components.js";
import networker from "./networker.js";
import checkMobile from "./checkmobile.js";

class App {
    main: HTMLDivElement;
    title: HTMLDivElement;
    body: HTMLDivElement;

    isMobile = checkMobile();

    constructor() {
        this.main = document.createElement("div");

        if (!this.isMobile) {
            components.flagNotMobile();
        }

        this.title = components.createTitleElm();
        this.body = components.createBodyElm();

        this.setup();
    }

    setup(): void {
        this.main.appendChild(this.title);
        this.main.appendChild(this.body);

        // {
        //     const div = document.createElement("div");
        //     div.style.height = "1000px";
        //     this.main.appendChild(div);
        // }

        this.main.classList.add("main");
        document.body.appendChild(this.main);
    }
}

const app = new App();