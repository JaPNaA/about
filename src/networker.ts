class Networker {
    static pendingRequests: Promise<Response>[] = [];
    static doneRequests: Promise<Response>[] = [];

    private static nocacheHeader: Headers = new Headers();
    private static nocacheFetchInit: Object;

    static init() {
        this.nocacheFetchInit = {
            method: "GET",
            headers: this.nocacheHeader
        };

        this.nocacheHeader.append("pragma", "no-cache");
        this.nocacheHeader.append("cache-control", "no-cache");
    }

    private static registerRequest(req: Promise<Response>): void {
        req.then(e => {
            this.doneRequests.push(req);
            this.pendingRequests.splice(this.pendingRequests.indexOf(req), 1);
            return e;
        });

        this.pendingRequests.push(req);
    }

    static get(path: string): Promise<Response> {
        let req = fetch(path);
        this.registerRequest(req);
        return req;
    }

    static getFresh(path: string): Promise<Response> {
        let req = fetch(path, this.nocacheFetchInit);
        this.registerRequest(req);
        return req;
    }
}

Networker.init();

export default Networker;