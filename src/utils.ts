let utils = {
    /**
     * gets the distance from point to 0, 0
     * @param x 
     * @param y 
     */
    pytha(x: number, y: number): number {
        return Math.sqrt(x * x + y * y);
    },

    /**
     * gets the sign (+/-) of number
     * @param x 
     */
    sign(x: number): number {
        return x < 0 ? -1 : 1;
    },


    /**
     * squares while preserving sign
     * @param x number to square
     */
    signedSquare(x: number): number {
        return utils.sign(x) * x * x;
    },

    /**
     * splits a string before delimeter
     * @param s string to split
     * @param d delimeter
     */
    splitBefore(s: string, d: RegExp): string[] {
        let str = s;
        let arr: string[] = [];

        while (str.length > 0) {
            let match = str.slice(1).match(d);
            if (!match) {
                arr.push(str);
                break;
            }

            if (match.index !== undefined) {
                let part = str.slice(0, match.index + 1);
                str = str.slice(match.index + 1);

                arr.push(part);
            } else {
                throw new TypeError("RegExp cannot be global");
            }
        }

        return arr;
    }
};

export default utils;